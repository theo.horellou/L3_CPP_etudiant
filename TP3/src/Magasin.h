//
// Created by thorellou on 23/03/2020.
//
#include <iostream>
#include <vector>
#include "Client.h"
#include "Produit.h"
#include "Location.h"

#ifndef TP_VIDE_MAGASIN_H
#define TP_VIDE_MAGASIN_H


class Magasin {
    std::vector<Client> _clients;
    std::vector<Produit> _produits;
    std::vector<Location> _locations;
    int _idCourantClient;
    int _idCourantProduit;

public:Magasin();
public:int nbClients() const;
public:void ajouterClient(const std::string & nom);
public:void afficherClients() const;
public:void supprimerClient(int idClient);

public:int nbProduits() const;
public:void ajouterProduit(const std::string & description);
public:void afficherProduits() const;
public:void supprimerProduit(int idProduit);

public:int nbLocations() const;
public:void ajouterLocation(int idClient, int idProduit);
public:void afficherLocations() const;
public:void supprimerLocation(int idClient, int idProduit);
public:bool trouverClientDansLocation(int idClient) const;
public:std::vector<int> calculerClientsLibres() const;
public:bool trouverProduitDansLocation(int idProduit) const;
public:std::vector<int> calculerProduitsLibres() const;
};


#endif //TP_VIDE_MAGASIN_H
