//
// Created by thorellou on 23/03/2020.
//

#ifndef TP_VIDE_LOCATION_H
#define TP_VIDE_LOCATION_H

struct Location {
    int _idClient;
    int _idProduct;

    Location();
    Location(int idClient, int idProduct);
    void afficherLocation() const;
};

#endif //TP_VIDE_LOCATION_H
