//
// Created by thorellou on 23/03/2020.
//

#include "Location.h"
#include "Client.h"
#include "Produit.h"
#include "Magasin.h"

#include <iostream>

int main() {
    Location loc(1,1);
    loc.afficherLocation();

    Client cli(1, "toto");
    cli.afficherClient();

    Produit pro(1, "toto");
    pro.afficherProduit();

    Magasin magasin;
    magasin.ajouterClient("Client 1");
    magasin.ajouterProduit("Produit 1");
    magasin.ajouterLocation(1,1);

    magasin.afficherClients();
    magasin.afficherProduits();
    magasin.afficherLocations();

    return 0;
}
