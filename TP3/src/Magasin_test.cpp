#include "Magasin.h"

#include <CppUTest/CommandLineTestRunner.h>
#include <iostream>
#include <exception>

using namespace std;

TEST_GROUP(GroupMagasin) { };

TEST(GroupMagasin, Magasin_Test)  {
    Magasin magasin;

    magasin.ajouterClient("test1");
    magasin.ajouterClient("test2");
    magasin.ajouterClient("test3");
    CHECK_EQUAL(3, magasin.nbClients());
    magasin.supprimerClient(2);
    CHECK_EQUAL(2, magasin.nbClients());
    magasin.afficherClients();

    try {
        magasin.supprimerClient(2);
    } catch(string& exception) {
        CHECK_EQUAL("Erreur : Ce Client n'existe pas.", exception);
    }

    magasin.ajouterProduit("test1");
    magasin.ajouterProduit("test2");
    magasin.ajouterProduit("test3");
    CHECK_EQUAL(3, magasin.nbProduits());
    magasin.supprimerProduit(2);
    CHECK_EQUAL(2, magasin.nbProduits());
    magasin.afficherProduits();

    try {
        magasin.supprimerProduit(2);
    } catch(string& exception) {
        CHECK_EQUAL("Erreur : Ce Produit n'existe pas.", exception);
    }

    magasin.ajouterLocation(1,1);
    magasin.ajouterLocation(1,2);
    magasin.ajouterLocation(1,3);
    CHECK_EQUAL(3, magasin.nbLocations());
    magasin.supprimerLocation(1,3);
    CHECK_EQUAL(2, magasin.nbLocations());
    magasin.afficherLocations();

    try {
        magasin.supprimerLocation(1, 3);
    } catch(string& exception) {
        CHECK_EQUAL("Erreur : Cette Location n'existe pas.", exception);
    }

    try {
        magasin.ajouterLocation(1, 1);
    } catch(string& exception) {
        CHECK_EQUAL("Erreur : Cette Location existe déjà.", exception);
    }

    std::vector<int> clientsLibres = magasin.calculerClientsLibres();
    CHECK_EQUAL(1, clientsLibres.size());

    std::vector<int> produitsLibres = magasin.calculerProduitsLibres();
    CHECK_EQUAL(1, produitsLibres.size());
};
