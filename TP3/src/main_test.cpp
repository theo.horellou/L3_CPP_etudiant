//
// Created by thorellou on 23/03/2020.
//

#include <CppUTest/CommandLineTestRunner.h>

int main(int argc, char ** argv) {
    return CommandLineTestRunner::RunAllTests(argc, argv);
}
