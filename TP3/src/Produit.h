//
// Created by thorellou on 23/03/2020.
//
#include <iostream>

#ifndef TP_VIDE_PRODUIT_H
#define TP_VIDE_PRODUIT_H


class Produit {
    int _id;
    std::string _description;

public:Produit(int id, const std::string & description);
public:int getId() const;
public:const std::string & getDescription() const;
public:void afficherProduit() const;
};


#endif //TP_VIDE_PRODUIT_H