//
// Created by thorellou on 23/03/2020.
//

#include "Produit.h"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupClient) { };

TEST(GroupClient, Client_Constructeur)  {
    Produit produit(1, "test");
    CHECK_EQUAL(1, produit.getId());
    CHECK_EQUAL("test", produit.getDescription());
}
