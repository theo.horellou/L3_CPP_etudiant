//
// Created by thorellou on 23/03/2020.
//
#include "Client.h"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupClient) { };

TEST(GroupClient, Client_Constructeur)  {
    Client client(1, "test");
    CHECK_EQUAL(1, client.getId());
    CHECK_EQUAL("test", client.getNom());
}
