//
// Created by thorellou on 23/03/2020.
//
#include <iostream>

#ifndef TP_VIDE_CLIENT_H
#define TP_VIDE_CLIENT_H

class Client {
    int _id;
    std::string _nom;

public:Client(int id, const std::string & nom);
public:int getId() const;
public:const std::string & getNom() const;
public:void afficherClient() const;
};


#endif //TP_VIDE_CLIENT_H