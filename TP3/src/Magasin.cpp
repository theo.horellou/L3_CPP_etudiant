//
// Created by thorellou on 23/03/2020.
//

#include "Magasin.h"

Magasin::Magasin() {
    _idCourantClient = 0;
    _idCourantProduit = 0;
}

int Magasin::nbClients() const {
    return _clients.size();
}

void Magasin::ajouterClient(const std::string &nom) {
    Client client(_idCourantClient, nom);
    _clients.push_back(client);
    _idCourantClient++;
}

void Magasin::afficherClients() const {
    for(int i = 0; i < nbClients(); i++){
        _clients.at(i).afficherClient();
    }
}

void Magasin::supprimerClient(int idClient) {
    int i = 0;
    try {
        while(_clients.at(i).getId() != idClient && i < nbClients())
        {
            i++;
        }
    } catch (std::exception& e){
        throw std::string("Erreur : Ce Client n'existe pas.");
    }
    _clients.erase(_clients.begin()+i);
}

int Magasin::nbProduits() const {
    return _produits.size();
}

void Magasin::ajouterProduit(const std::string &description) {
    Produit produit(_idCourantProduit, description);
    _produits.push_back(produit);
    _idCourantProduit++;
}

void Magasin::afficherProduits() const {
    for(int i = 0; i < nbProduits(); i++){
        _produits.at(i).afficherProduit();
    }
}

void Magasin::supprimerProduit(int idProduit) {
    int i = 0;
    try {
        while(_produits.at(i).getId() != idProduit && i < nbProduits())
        {
            i++;
        }
    } catch (std::exception& e){
        throw std::string("Erreur : Ce Produit n'existe pas.");
    }
    _produits.erase(_produits.begin()+i);
}

int Magasin::nbLocations() const {
    return _locations.size();
}

void Magasin::ajouterLocation(int idClient, int idProduit) {
    Location location(idClient, idProduit);

    int i = 0;
    bool found = false;
    while (!found && i < nbLocations()) {
        if(_locations.at(i)._idClient == idClient && _locations.at(i)._idProduct == idProduit){
            found = true;
        } else {
            i++;
        }
    }
    if(found){
        throw std::string("Erreur : la Location existe déjà.");
    } else {
        _locations.push_back(location);
    }
}

void Magasin::afficherLocations() const {
    for(int i = 0; i < nbLocations(); i++){
        _locations.at(i).afficherLocation();
    }
}

void Magasin::supprimerLocation(int idClient, int idProduit) {
    int i = 0;
    try {
        while(_locations.at(i)._idClient != idClient && _locations.at(i)._idProduct != idProduit && i < nbLocations())
        {
            i++;
        }
    } catch (std::exception& e){
        throw std::string("Erreur : Cette Location n'existe pas.");
    }
    _locations.erase(_locations.begin()+i);
}

bool Magasin::trouverClientDansLocation(int idClient) const {
    for(int i = 0; i < nbLocations(); i++){
        if(_locations.at(i)._idClient == idClient){
            return true;
        }
    }
    return false;
}

std::vector<int> Magasin::calculerClientsLibres() const {
    std::vector<int> clientsLibres;
    for(int i = 0; i < nbClients(); i++){
        if(!trouverClientDansLocation(i)){
            clientsLibres.push_back(_clients.at(i).getId());
        }
    }
    return clientsLibres;
}

bool Magasin::trouverProduitDansLocation(int idProduit) const {
    for(int i = 0; i < nbLocations(); i++){
        if(_locations.at(i)._idProduct == idProduit){
            return true;
        }
    }
    return false;
}

std::vector<int> Magasin::calculerProduitsLibres() const {
    std::vector<int> produitsLibres;
    for(int i = 0; i < nbProduits(); i++){
        if(!trouverProduitDansLocation(i)){
            produitsLibres.push_back(_produits.at(i).getId());
        }
    }
    return produitsLibres;
}
