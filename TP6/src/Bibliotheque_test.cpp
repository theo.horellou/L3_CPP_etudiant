#include "Bibliotheque.hpp"

#include <sstream>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupBibliotheque) { };

TEST(GroupBibliotheque, Bibliotheque_trierParAuteurEtTitre_1) 
{
    Bibliotheque b;
    b.push_back(Livre("titre","auteur",2000));
    b.push_back(Livre("titre2","auteur2",2001));
    b.push_back(Livre("titre3","auteur3",2002));
    b.trierParAuteurEtTitre();
    CHECK(b[0] == Livre("titre","auteur",2000));
    CHECK(b[1] == Livre("titre2","auteur2",2001));
    CHECK(b[2] == Livre("titre3","auteur3",2002));
}

TEST(GroupBibliotheque, Bibliotheque_trierParAuteurEtTitre_2) 
{
    Bibliotheque b;
    b.push_back(Livre("titre2","auteur2",2001));
    b.push_back(Livre("titre3","auteur",2002));
    b.push_back(Livre("titre","auteur",2000));
    b.trierParAuteurEtTitre();
    CHECK(b[0] == Livre("titre","auteur",2000));
    CHECK(b[1] == Livre("titre3","auteur",2002));
    CHECK(b[2] == Livre("titre2","auteur2",2001));
}

TEST(GroupBibliotheque, Bibliotheque_trierParAnnee_1) 
{
    Bibliotheque b;
    b.push_back(Livre("titre","auteur",2000));
    b.push_back(Livre("titre2","auteur2",2001));
    b.push_back(Livre("titre3","auteur3",2002));
    b.trierParAnnee();
    CHECK(b[0] == Livre("titre2","auteur2",2001));
    CHECK(b[1] == Livre("titre3","auteur3",2002));
    CHECK(b[2] == Livre("titre","auteur",2000));
}

TEST(GroupBibliotheque, Bibliotheque_fichier_1 ) 
{
    Bibliotheque b1;
    b1.push_back(Livre("titre","auteur",2000));
    b1.push_back(Livre("titre2","auteur2",2001));
    b1.push_back(Livre("titre3","auteur3",2002));
    b1.ecrireFichier("biblio.txt");
    Bibliotheque b2;
    b2.lireFichier("./biblio.txt");
    CHECK(b2[0] == Livre("titre","auteur",2000));
    CHECK(b2[1] == Livre("titre2","auteur2",2001));
    CHECK(b2[2] == Livre("titre3","auteur3",2002));
}

TEST(GroupBibliotheque, Bibliotheque_charger_1 )
{
    Bibliotheque b;
    try
    {
        b.lireFichier("biblio_inex.txt");
		FAIL( "exception non levee" );
    }
	catch (const std::string& str) 
    {
        CHECK_EQUAL(str, "erreur, lecture impossible");
	}
}