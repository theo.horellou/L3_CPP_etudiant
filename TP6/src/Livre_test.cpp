#include "Livre.hpp"

#include <sstream>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupLivre) { };

TEST(GroupLivre, Livre_constructeur_1) 
{
	Livre livre("titre", "auteur", 2000);
	CHECK(livre.getTitre() == std::string("titre"));
	CHECK(livre.getAuteur() == "auteur");
	CHECK_EQUAL(livre.getAnnee(), 2000);
}

TEST(GroupLivre, Livre_constructeur_2) 
{
	try 
    {
        Livre livre("titre1;", "auteur1", 2000);
		FAIL( "exception non levee" );
	}
	catch (const std::string& str) 
    {
		CHECK_EQUAL(str, "erreur : le point virgule n'est pas accepté");
	}
}

TEST(GroupLivre, Livre_constructeur_3) 
{
	try 
    {
        Livre livre("titre1", "auteur1;", 2000);
		FAIL( "exception non levee" );
	}
	catch (const std::string& str) 
    {
		CHECK_EQUAL(str, "erreur : le point virgule n'est pas accepté");
	}
}

TEST(GroupLivre, Livre_constructeur_4) 
{
	try 
    {
        Livre livre("titre1", "auteur1\n", 2000);
		FAIL( "exception non levee" );
	}
	catch (const std::string& str) 
    {
		CHECK_EQUAL(str, "erreur : le retour chariot n'est pas accepté");
	}
}

TEST(GroupLivre, Livre_inferieur_pp) 
{
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre","auteur",1), false);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre","auteur",0), false);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre","auteur",2), false);
}

TEST(GroupLivre, Livre_inferieur_pz) 
{
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre","auteur1",1), false);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre","auteur1",0), false);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre","auteur1",2), false);
}

TEST(GroupLivre, Livre_inferieur_pm) 
{
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre","auteur2",1), true);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre","auteur2",0), true);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre","auteur2",2), true);
}

TEST(GroupLivre, Livre_inferieur_zp) 
{
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre1","auteur",1), false);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre1","auteur",0), false);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre1","auteur",2), false);
}

TEST(GroupLivre, Livre_inferieur_zz) 
{
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre1","auteur1",1), false);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre1","auteur1",0), false);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre1","auteur1",2), false);
}

TEST(GroupLivre, Livre_inferieur_zm) 
{
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre1","auteur2",1), true);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre1","auteur2",0), true);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre1","auteur2",2), true);
}

TEST(GroupLivre, Livre_inferieur_mp) 
{
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre2","auteur",1), false);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre2","auteur",0), false);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre2","auteur",2), false);
}

TEST(GroupLivre, Livre_inferieur_mz) 
{
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre2","auteur1",1), true);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre2","auteur1",0), true);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre2","auteur1",2), true);
}

TEST(GroupLivre, Livre_inferieur_mm) 
{
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre2","auteur2",1), true);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre2","auteur2",0), true);
	CHECK_EQUAL(Livre("titre1","auteur1",1)<Livre("titre2","auteur2",2), true);
}

TEST(GroupLivre, Livre_egalite_1) 
{
	CHECK_EQUAL(Livre("titre1","auteur1",1)==Livre("titre1","auteur1",1), true);
}

TEST(GroupLivre, Livre_egalite_2) 
{
	CHECK_EQUAL(Livre("titre1","auteur1",1)==Livre("titre2","auteur1",1), false);
	CHECK_EQUAL(Livre("titre1","auteur1",1)==Livre("titre1","auteur2",1), false);
	CHECK_EQUAL(Livre("titre1","auteur1",1)==Livre("titre1","auteur1",2), false);
}

TEST(GroupLivre, Livre_entree_1) 
{
	Livre livre;
	std::stringstream s("titre;auteur;12");
	s >> livre;
	CHECK(livre.getTitre() == "titre");
	CHECK(livre.getAuteur() == "auteur");
	CHECK_EQUAL(livre.getAnnee(), 42);
}

TEST(GroupLivre, Livre_sortie_1) 
{
	Livre livre("titre", "auteur", 42);
	std::stringstream s;
	s << livre;
	CHECK_EQUAL(std::string("titre;auteur;10"), s.str());
}

