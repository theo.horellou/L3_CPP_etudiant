//
// Created by thorellou on 26/03/2020.
//

#ifndef TP6_BIBLIOTHEQUE_H
#define TP6_BIBLIOTHEQUE_H

#include <vector>
#include "Livre.hpp"

class Bibliotheque : public std::vector<Livre> {
public:
    void afficher() const;
    void trierParAuteurEtTitre();
    void trierParAnnee();
    void lireFichier(const std::string &nomFichier);
    void ecrireFichier(const std::string &nomFichier);
};

bool sortBibliotheque(const Livre &livreA, const Livre &livreB);
bool sortBibliothequeAnnee(const Livre &livreA, const Livre &livreB);

#endif //TP6_BIBLIOTHEQUE_H
