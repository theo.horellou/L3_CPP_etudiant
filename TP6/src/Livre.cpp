//
// Created by thorellou on 26/03/2020.
//

#include "Livre.hpp"

Livre::Livre(){
    _titre = "";
    _auteur = "";
    _annee = 0;
}

Livre::Livre(const std::string &titre, const std::string &auteur, int annee){
    if(isValid(titre)==1){
        throw std::string("erreur : le retour chariot n'est pas accepté");
    }else if(isValid(titre)==2){
        throw std::string("erreur : le point virgule n'est pas accepté");
    }else if(isValid(titre)==3){
        throw std::string("erreur : le point virgule et le retour chariot ne sont pas acceptés");
    }
    if(isValid(auteur)==1){
        throw std::string("erreur : le retour chariot n'est pas accepté");
    }else if(isValid(auteur)==2){
        throw std::string("erreur : le point virgule n'est pas accepté");
    }else if(isValid(auteur)==3){
        throw std::string("erreur : le point virgule et le retour chariot ne sont pas acceptés");
    }
    _annee = annee;
}

const std::string & Livre::getTitre() const {
    return _titre;
}



int Livre::getAnnee() const {
    return _annee;
}

const std::string & Livre::getAuteur() const {
    return _auteur;
}

bool Livre::operator==(const Livre &livre) const {
    return _titre == livre._titre && _auteur == livre._auteur && _annee == livre._annee;
}

bool Livre::operator<(const Livre &livre) const {
    if(livre.getAuteur() == getAuteur()){
        return livre.getTitre() > getTitre();
    } else {
        return livre.getAuteur() > getAuteur();
    }
}

std::ostream &operator<<(std::ostream &os, const Livre &livre) {
    os << livre._titre << ";" << livre._auteur << ";" << livre._annee;
    return os;
}

std::istream& operator >>(std::istream& is, Livre &livre) {
    char str[99];
    is.getline(str, 99, ';');
    livre._titre = str;
    is.getline(str, 99, ';');
    livre._auteur = str;
    is.getline(str, 99, ';');
    livre._annee = atoi(str);
    return is;
}

int isValid(const std::string &value) {
    std::size_t retourChariot, pointVirgule;

    retourChariot = value.find('\n');
    pointVirgule = value.find(';');

    int retour = 0;

    if (retourChariot != std::string::npos) {
        retour += 1;
    }
    if (pointVirgule != std::string::npos) {
        retour += 2;
    }

    return retour;
}