//
// Created by thorellou on 26/03/2020.
//

#include "Bibliotheque.hpp"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <sstream>

void Bibliotheque::afficher() const {

    for(const Livre &livre: *this){
        std::cout << livre << std::endl;
    }

}


void Bibliotheque::trierParAuteurEtTitre() {
    sort(begin(), end(), sortBibliotheque);
}


bool sortBibliotheque(const Livre &livreA, const Livre &livreB){
    return livreA < livreB;
}


void Bibliotheque::trierParAnnee() {
    sort(begin(), end(), sortBibliothequeAnnee);
}


bool sortBibliothequeAnnee(const Livre &livreA, const Livre &livreB){
    return livreA.getAnnee() < livreB.getAnnee();
}


void Bibliotheque::lireFichier(const std::string &nomFichier) {

    std::ifstream file;
    file.open(nomFichier, std::ifstream::in);

    if(!file.is_open()){
        file.close();
        throw std::string("erreur, lecture impossible");
    }
    char str[256];

    while(file.getline(str, 256, '\n')){
        std::stringstream strstream;
        strstream << str;
        Livre l;
        strstream >> l;
        push_back(l);
    }

    file.close();
}

void Bibliotheque::ecrireFichier(const std::string &nomFichier) {

    std::ofstream file;
    file.open(nomFichier);
    for(const Livre &livre: *this){
        file << livre << std::endl;
    }

    file.close();
}