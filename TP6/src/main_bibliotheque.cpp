
#include <iostream>
#include "Bibliotheque.hpp"

int main() {
    Bibliotheque b;
    Livre l1("1984", "George Orwell", 1949);
    Livre l2("Fahrenheit 451", "Ray Bradbury", 1955);

    b.push_back(l1);
    b.push_back(l2);

    std::cout << "Bibliothèque :" << std::endl;
    b.afficher();

    b.trierParAuteurEtTitre();

    std::cout << std::endl << "Bibliothèque triée : " << std::endl;
    b.afficher();

    b.trierParAnnee();

    std::cout << std::endl << "Bibliothèque triée par année : " << std::endl;
    b.afficher();
    return 0;
}

