//
// Created by thorellou on 26/03/2020.
//

#ifndef TP6_LIVRE_H
#define TP6_LIVRE_H

#include <iostream>

class Livre {
private:
    std::string _titre;
    std::string _auteur;
    int _annee;
public:
    Livre();
    Livre(const std::string & titre, const std::string & auteur, int annee);
    const std::string & getTitre() const;
    const std::string & getAuteur() const;
    int getAnnee() const;

    bool operator<(const Livre &livre) const;
    bool operator==(const Livre &livre) const;
    friend std::ostream &operator<<(std::ostream &os, const Livre &livre);
    friend std::istream &operator>>(std::istream &is, Livre &livre);
};

int isValid(const std::string &value);

#endif //TP6_LIVRE_H
