#ifndef VECTEUR3_HPP_
#define VECTEUR3_HPP_


typedef struct
{
    float x, y, z;
    void afficher();
} vector3D;

void afficher(vector3D vector);
void norme(vector3D vector);
void produitScal(vector3D vector_1, vector3D vector_2);
void addition(vector3D vector_1, vector3D vector_2);

#endif