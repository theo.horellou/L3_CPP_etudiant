#include "Fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>
TEST_GROUP(GroupFibonacci) {};
TEST(GroupFibonacci, testFibonacci_1)
{
int resultat = fibonacciRecursif(1);
CHECK_EQUAL(1, resultat);
}
TEST(GroupFibonacci, testFibonacci_2)
{
int resultat = fibonacciRecursif(2);
CHECK_EQUAL(1, resultat);
}
TEST(GroupFibonacci, testFibonacci_3)
{
int resultat = fibonacciRecursif(3);
CHECK_EQUAL(2, resultat);
}
TEST(GroupFibonacci, testFibonacci_4)
{
int resultat = fibonacciRecursif(4);
CHECK_EQUAL(3, resultat);
}
TEST(GroupFibonacci, testFibonacci_5)
{
int resultat = fibonacciRecursif(5);
CHECK_EQUAL(5, resultat);
}
TEST(GroupFibonacci, testFibonacciI_1)
{
int resultat = fibonacciIteratif(1);
CHECK_EQUAL(1, resultat);
}
TEST(GroupFibonacci, testFibonacciI_2)
{
int resultat = fibonacciIteratif(2);
CHECK_EQUAL(1, resultat);
}
TEST(GroupFibonacci, testFibonacciI_3)
{
int resultat = fibonacciIteratif(3);
CHECK_EQUAL(2, resultat);
}
TEST(GroupFibonacci, testFibonacciI_4)
{
int resultat = fibonacciIteratif(4);
CHECK_EQUAL(3, resultat);
}
TEST(GroupFibonacci, testFibonacciI_5)
{
int resultat = fibonacciIteratif(5);
CHECK_EQUAL(5, resultat);
}
