//
// Created by thorellou on 06/03/2020.
//

#ifndef LISTE_H
#define LISTE_H

struct Noeud
{
    int _valeur;
    Noeud * _suivant;
};

struct Liste
{
    Noeud * _tete;

    Liste();
    ~Liste();
    void ajouterDevant(int valeur);
    int getTaille() const;
    int getElement(int indice) const;
};

#endif

