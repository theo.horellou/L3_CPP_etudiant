//
// Created by thorellou on 06/03/2020.
//

#include <iostream>
#include "Liste.h"

using namespace std;

Liste::Liste(){
    _tete = nullptr;
}

Liste::~Liste(){
    Noeud * next = _tete;
    Noeud * previous;
    while(next != nullptr){
        previous = next;
        next = next->_suivant;
        delete previous;
    }
}

void Liste::ajouterDevant(int valeur){
    Noeud * _new = new Noeud();
    _new->_valeur = valeur;
    _new->_suivant = _tete;
    _tete = _new;
}

int Liste::getTaille() const{
    Noeud * next = _tete;
    int i = 0;
    while(next != nullptr){
        i++;
        next = next->_suivant;
    }
    return i;
}

int Liste::getElement(int indice) const{
    Noeud * next = _tete;
    while(indice != 0 && next != nullptr){
        indice--;
        next = next->_suivant;
    }
    if(next == nullptr){
        return -1;
    }
    return next->_valeur;
}
