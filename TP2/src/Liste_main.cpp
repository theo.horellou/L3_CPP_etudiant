//
// Created by thorellou on 06/03/2020.
//

#include "Liste.h"

#include <iostream>

int main() {
    Liste list;

    list.ajouterDevant(13);
    list.ajouterDevant(37);

    for(int i = 0; i < 2; i++) {
        std::cout << list.getElement(i) << std::endl;
    }

    return 0;
}
