#include <iostream>
#include "Liste.h"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupListe) { };

TEST(GroupListe, Liste_Constructeur) {
    Liste list = Liste();
    CHECK_EQUAL(list._tete, 0);
}

TEST(GroupListe, Liste_AjouterDevant) {
    Liste list = Liste();
    list.ajouterDevant(5);
    CHECK_EQUAL(list._tete->_valeur, 5);
}

TEST(GroupListe, Liste_GetTaille) {
    Liste list = Liste();
    list.ajouterDevant(5);
    CHECK_EQUAL(list.getTaille(), 1);
}

TEST(GroupListe, Liste_GetElement) {
    Liste list = Liste();
    list.ajouterDevant(5);
    CHECK_EQUAL(list.getElement(0), 5);
}
