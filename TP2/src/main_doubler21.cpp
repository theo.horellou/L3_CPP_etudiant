#include "Doubler.hpp"

#include <iostream>

int main() {
	// Pointeurs
    int a;
    a = 42;
    std::cout << a << std::endl;
    
    int * p;
    p = &a;
    *p = 37;
    
    std::cout << a << std::endl;
    
    //Allocation dynamique
    int * t = new int[10];
    t[3] = 42;
    
    std::cout << t[3] << std::endl;
    
    delete [] t;
    
    t = nullptr;
    
    return 0;
}

