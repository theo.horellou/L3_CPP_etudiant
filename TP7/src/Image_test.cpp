#include "Image.hpp"
#include <CppUTest/CommandLineTestRunner.h>

//MemoryLeakWarningPlugin::turnOffNewDeleteOverloads()

TEST_GROUP(GroupeImage) { };

TEST(GroupeImage, getters)  {
    Image mon_image(2,3);
    CHECK_EQUAL(mon_image.getLargeur(), 2);
    CHECK_EQUAL(mon_image.getHauteur(), 3);
}

TEST(GroupeImage, setters)  {
    Image mon_image(2,3);
    mon_image.setPixel(1,1,100);
    CHECK_EQUAL(mon_image.getPixel(1,1), 100);
}

TEST(GroupeImage, PixelParReference) {

    Image mon_image(2, 3);
    mon_image.setPixel(1,1,10);


    int &temp = mon_image.getPixelRef(1, 1);
    CHECK_EQUAL( temp, 10);
    temp = 18;
    CHECK_EQUAL(temp, 18);
}
