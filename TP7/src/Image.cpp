#include "Image.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

Image::Image(int largeur,int hauteur){
    _largeur=largeur;
    _hauteur=hauteur;
    _pixels = new int[largeur*hauteur];
}

Image::~Image() {
    delete [] _pixels;
}

Image::Image(const Image & img){
    _largeur=img.getLargeur();
    _hauteur=img.getHauteur();
    _pixels = new int[img.getLargeur()*img.getHauteur()];

    for (int i=0; i < _largeur * _hauteur ; i++)
    {
        _pixels[i]=i;
    }

}

int Image::getLargeur()const{
    return _largeur;
}

int Image::getHauteur()const{
    return _hauteur;
}

int &Image::getPixelRef(int i,int j)
{
    return _pixels[i+_largeur*j];
}

const int & Image::getPixelRefbis(int i, int j) const {
    return _pixels[i+_largeur*j];
}


void ecrirePnm(const Image & img, const std::string & nomFichier){

    std::ofstream f(nomFichier);

    f << "P2 "<< img.getLargeur()<<" "<<img.getHauteur()<< " 255"<<std::endl;

    for(int j = 0 ; j< img.getHauteur(); j++)
    {
        for(int i=0; i< img.getLargeur(); i++)
        {
            f << img.getPixelRefbis(i,j) << " ";
        }

        f << std::endl;
    }
    f.close();
}

void remplir(Image &img){

    for (int j=0;j< img.getHauteur();j++)
    {
        for (int i=0; i<img.getLargeur();i++)
        {

            double couleur = cos (i*3.14/180.f);
            couleur = ((couleur+1)/2)*255;
            img.getPixelRef(i,j)= couleur;

        }
    }
}

Image bordure(const Image & img, int couleur, int epaisseur) {
    Image temp(img.getLargeur() + epaisseur, img.getHauteur() + epaisseur);

    for(int j = 0; j < temp.getHauteur(); j++)
    {
        for(int i = 0; i < temp.getLargeur(); i++)
        {
            if( i <= epaisseur || j <= epaisseur ||  i >= temp.getLargeur()-epaisseur || j >= temp.getHauteur()-epaisseur)
            {
                temp.getPixelRef(i, j) = couleur;
            }
        }
    }
    return temp;
}


