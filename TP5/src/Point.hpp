//
// Created by thorellou on 23/03/2020.
//

#ifndef TP_VIDE_POINT_H
#define TP_VIDE_POINT_H
#include <iostream>

using namespace std;

struct Point {
    int _x;
    int _y;

    Point(): _x(0), _y(0){}
    Point(int x, int y) : _x(x), _y(y) {}
    string afficher() const{
        return to_string(_x) + " - " + to_string(_y);
    }
};

#endif //TP_VIDE_POINT_H
