#include <gtkmm.h>
#include <iostream>
#include "ViewerFigures.hpp"
#include "ZoneDessin.hpp"


int main(int argc, char *argv[]) {
    ViewerFigures viewerFigures(argc, argv);
    viewerFigures.run();

    return 0;
}
