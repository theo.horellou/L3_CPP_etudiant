//
// Created by thorellou on 24/03/2020.
//

#ifndef TP_VIDE_FIGUREGEOMETRIQUE_H
#define TP_VIDE_FIGUREGEOMETRIQUE_H

#include <cairomm/refptr.h>
#include <cairomm/context.h>
#include "Couleur.h"

class FigureGeometrique {

protected:
    Couleur _couleur;
public:
    FigureGeometrique(const Couleur &couleur);

    Couleur getCouleur() const;

    virtual void afficher(const Cairo::RefPtr<Cairo::Context> context) const;

    virtual ~FigureGeometrique() {};
};


#endif //TP_VIDE_FIGUREGEOMETRIQUE_H

