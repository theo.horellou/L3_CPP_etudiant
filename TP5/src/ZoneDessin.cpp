#include "ZoneDessin.hpp"
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"

using namespace std;

ZoneDessin::ZoneDessin() {

    _figures = {
            //new Ligne(Couleur(1, 0, 0), Point(0, 0), Point(100, 200)),
    };
    add_events(Gdk::BUTTON_PRESS_MASK);

    signal_button_press_event().connect(sigc::mem_fun(*this, &ZoneDessin::gererClic));
};

ZoneDessin::~ZoneDessin() {

    for (FigureGeometrique *fG : _figures) {
        delete fG;
    }
}

bool ZoneDessin::on_draw(const Cairo::RefPtr<Cairo::Context> & context) {
    for (FigureGeometrique *fG : _figures) {
        fG->afficher(context);
    }
}

bool ZoneDessin::gererClic(GdkEventButton* event)
{

    if(event->type == GDK_BUTTON_PRESS)
    {

        float c_r = std::rand()%101/100.0f;

        float c_g = std::rand()%101/100.0f;
        float c_b = std::rand()%101/100.0f;

        Couleur c(c_r,c_g,c_b);


        std::cout << "x" << std::endl;

        if(event->button == 1)
        {
            Point centre(event->x,event->y);

            _figures.push_back(new PolygoneRegulier(c, centre, rand()%100, rand()%10));


        }
        get_window()->invalidate(false);

    }

    return true;
}
