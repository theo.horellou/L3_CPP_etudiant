//
// Created by thorellou on 23/03/2020.
//

#include <iostream>
#include "Point.hpp"
#include "Couleur.h"
#include "Ligne.hpp"
#include "FigureGeometrique.hpp"

using namespace std;

Ligne::Ligne(const Couleur &couleur, const Point &p0, const Point &p1) :
        FigureGeometrique(couleur), _p0(p0), _p1(p1) {}

void Ligne::afficher(const Cairo::RefPtr<Cairo::Context> context) const {
    cout << "Ligne :" << _couleur.afficher() << " " << _p0._x << "_" << _p0._y << " " << _p1._x << "_" << _p1._y
         << endl;

//    cout << "Ligne :" << getCouleur()._r << "_" << getCouleur()._g << "_" << getCouleur()._b
//         << " " << _p0._x << "_" << _p0._y << " " << _p1._x << "_" << _p1._y << endl;
    context->set_source_rgb(_couleur._r, _couleur._g, _couleur._b);
    context->set_line_width(10.0);

    context->move_to(_p0._x, _p0._y);
    context->line_to(_p1._x, _p1._y);
    context->stroke();

}

const Point &Ligne::getP0() const {
    return _p0;
}

const Point &Ligne::getP1() const {
    return _p1;
}


