//
// Created by thorellou on 23/03/2020.
//

#include <vector>
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"
#include "FigureGeometrique.hpp"

int main(int argc, char **argv) {
    vector<FigureGeometrique *> fGs = {
            new Ligne(Couleur(1, 0, 0), Point(0, 0), Point(100, 200)),
            new PolygoneRegulier(Couleur(0, 1, 0), Point(0, 0), 1, 4)
    };

    for (FigureGeometrique *fG : fGs) {
        fG->afficher();
    }

    for (FigureGeometrique *fig : fGs) {
        delete fig;
    }

    fGs.clear();
}
