//
// Created by thorellou on 23/03/2020.
//

#ifndef TP_VIDE_LIGNE_H
#define TP_VIDE_LIGNE_H

#include "Point.hpp"
#include "Couleur.h"
#include "FigureGeometrique.hpp"

class Ligne : public FigureGeometrique {
private:
    Point _p0;
    Point _p1;

public:
    Ligne(const Couleur &couleur, const Point &p0, const Point &p1);

    virtual void afficher(const Cairo::RefPtr<Cairo::Context> context) const override;

    const Point &getP0() const;

    const Point &getP1() const;
};



#endif //TP_VIDE_LIGNE_H
