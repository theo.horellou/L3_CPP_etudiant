//
// Created by thorellou on 24/03/2020.
//

#ifndef TP_VIDE_FIGUREGEOMETRIQUE_H
#define TP_VIDE_FIGUREGEOMETRIQUE_H

#include "Couleur.h"

class FigureGeometrique {

protected:
    Couleur _couleur;
public:
    FigureGeometrique(const Couleur &couleur);
    virtual ~FigureGeometrique(){}
    Couleur getCouleur() const;
    virtual void afficher() const;

};

#endif //TP_VIDE_FIGUREGEOMETRIQUE_H

