//
// Created by thorellou on 24/03/2020.
//

#ifndef TP4_POLYGONEREGULIER_H
#define TP4_POLYGONEREGULIER_H

#include "FigureGeometrique.h"
#include "Point.h"
#include <vector>


class PolygoneRegulier : public FigureGeometrique {
private:
    int _nbPoints;
    Point* _points;
public:
    PolygoneRegulier(const Couleur &couleur, const Point &centre, int rayon, int nbCotes);
    ~PolygoneRegulier();
    virtual void afficher() const override;

    int getNbPoints() const;

    const Point &getPoint(int indice) const;
};

#endif //TP4_POLYGONEREGULIER_H
