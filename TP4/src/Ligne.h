//
// Created by thorellou on 23/03/2020.
//

#ifndef TP_VIDE_LIGNE_H
#define TP_VIDE_LIGNE_H

#include "Point.h"
#include "Couleur.h"
#include "FigureGeometrique.h"

class Ligne : public FigureGeometrique {
private:
    Point _p0;
    Point _p1;

public:
    Ligne(const Couleur &couleur, const Point &p0, const Point &p1);
    ~Ligne(){}
    virtual void afficher() const override;

    const Point &getP0() const;

    const Point &getP1() const;
};

#endif //TP_VIDE_LIGNE_H
