//
// Created by thorellou on 23/03/2020.
//

#include "FigureGeometrique.h"
#include "Couleur.h"

using namespace std;

FigureGeometrique::FigureGeometrique(const Couleur &couleur) :
        _couleur(couleur) {}

Couleur FigureGeometrique::getCouleur() const {
    return _couleur;
}

void FigureGeometrique::afficher() const {
}
