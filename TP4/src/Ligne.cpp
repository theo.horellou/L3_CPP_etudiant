//
// Created by thorellou on 23/03/2020.
//

#include <iostream>
#include "Point.h"
#include "Couleur.h"
#include "Ligne.h"
#include "FigureGeometrique.h"

using namespace std;

Ligne::Ligne(const Couleur &couleur, const Point &p0, const Point &p1) :
        FigureGeometrique(couleur), _p0(p0), _p1(p1) {}

void Ligne::afficher() const {
    cout << "Ligne :" << _couleur.afficher() <<" " << _p0._x << " - " << _p0._y << " " << _p1._x << " - " << _p1._y << endl;

}

const Point &Ligne::getP0() const {
    return _p0;
}

const Point &Ligne::getP1() const {
    return _p1;
}

