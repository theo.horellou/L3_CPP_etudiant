//
// Created by thorellou on 23/03/2020.
//

#ifndef TP_VIDE_COULEUR_H
#define TP_VIDE_COULEUR_H

#include <iostream>

using namespace std;

struct Couleur {
    double _r;
    double _g;
    double _b;
    Couleur(double r, double g, double b) : _r(r), _g(g), _b(b) {}

    string afficher() const {
        return to_string(_r) + " - " + to_string(_g) + " - " + to_string(_b);
    }
};

#endif //TP_VIDE_COULEUR_H
