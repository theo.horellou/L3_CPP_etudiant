//
// Created by thorellou on 23/03/2020.
//

#include <cmath>
#include <iostream>
#include "PolygoneRegulier.h"
#include "Point.h"

using namespace std;

PolygoneRegulier::PolygoneRegulier(const Couleur &couleur, const Point &centre, int rayon, int nbCotes) :
        FigureGeometrique(couleur), _nbPoints(nbCotes) {
    _points = new Point[_nbPoints];
    for (int i = 0; i < _nbPoints; ++i) {
        float theta = i * 2 * M_PI / (float) _nbPoints;
        int x = rayon * cos(theta) + centre._x;
        int y = rayon * sin(theta) + centre._y;
        _points[i] = {x, y};
    }
}

PolygoneRegulier::~PolygoneRegulier() {
    delete[]_points;
}

void PolygoneRegulier::afficher() const {
    string message = _couleur.afficher();

    for (int i = 0; i < _nbPoints; ++i) {
        message = message + " " + _points[i].afficher();
    }

    cout << "PolygoneRegulier: = " << message << endl;
}

int PolygoneRegulier::getNbPoints() const {
    return _nbPoints;
}

const Point &PolygoneRegulier::getPoint(int indice) const {
    return _points[indice];
}

